﻿namespace Twc.Util.Task


type ITask =
  abstract Start: unit -> unit
  abstract Terminate: unit -> unit

