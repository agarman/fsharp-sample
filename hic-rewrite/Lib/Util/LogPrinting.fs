﻿module  Twc.Util.Logging.LogPrinting


module Union =
  // This union stuff belongs elsewhere.
  open Microsoft.FSharp.Reflection

  let GetName (x:'a)   = match FSharpValue.GetUnionFields(x, typeof<'a>) with case, _ -> case.Name
  let GetNames<'ty> () = FSharpType.GetUnionCases(typeof<'ty>) |> Array.map (fun info -> info.Name)


let rec pprintMessage (m: Message) = 
  match m with 
  | Simple o -> string o
  | WithException (o, e) -> sprintf "Exception: %s with %s" (string e) (string o)
  | WithType (o, t)      -> sprintf "Type: %s logged %s" (t.Name) (string o)
  | Complex (o, m)       -> sprintf "%s logged %s" (string o) (pprintMessage m)


let pprintLevel (l: Level) = Union.GetName l


let pprintLogEvent (e: LogEvent) =
  sprintf "Log %s: %s" (pprintLevel e.level) (pprintMessage e.message)

