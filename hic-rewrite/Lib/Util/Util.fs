﻿module Twc.Util.Util

open System.Threading.Tasks

let contains fn l =
  let r = Seq.tryFind fn l
  match r with
  | Some _ -> true
  | None   -> false

let has e = contains ((=) e)

let hasAll es l = es |> Seq.forall (fun e -> has e l)

let rec hasAny es l =
  match es with
  | []   -> false
  | h::t -> if has h l then true else hasAny t l

let pprintList hdr ftr sep elemPrinter l =
  hdr + String.concat sep (Seq.map elemPrinter l) + ftr

let pplist elemPrinter l = pprintList "[" "]" ", " elemPrinter l

let subseq start cnt l =
  Seq.take cnt (Seq.skip start l)

let dropCntAt cnt pos l =
  let pre = Seq.take pos l
  let post = Seq.skip (pos + cnt) l
  Seq.append pre post

let dropAt pos l = dropCntAt 1 pos l

let mapParallel (fn : 'a -> 'b) (l : seq<'a>) =
  let func e = new System.Func<'b> (fun () -> fn e)
  let spawn e = Task.Factory.StartNew<'b> (func e)
  let tasks = Seq.map spawn l
  tasks |> Seq.map (fun t -> t.Result)

let asSideEffect fn v = fn v |> ignore; v

let constant a b = a

/// Copies all values from first array into second starting at specified position.
let blit ar ar2 pos = Array.blit ar 0 ar2 pos ar.Length

let dosafe fn onError = 
  try
    fn ()
  with ex ->
    onError ex

