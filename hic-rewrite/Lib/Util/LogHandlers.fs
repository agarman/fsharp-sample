﻿module Twc.Util.Logging.LogHandlers


open Twc.Util.Logging
open Twc.Util.Logging.LogPrinting


let mutable errorConsoleColor = System.ConsoleColor.Red
let mutable warnConsoleColor  = System.ConsoleColor.Yellow
let mutable infoConsoleColor  = System.ConsoleColor.Gray
let mutable debugConsoleColor = System.ConsoleColor.Cyan


let private consoleColor l =
  match l with
  | ERROR -> errorConsoleColor
  | WARN  -> warnConsoleColor
  | INFO  -> infoConsoleColor
  | DEBUG -> debugConsoleColor
  | _     -> System.Console.ForegroundColor


let console (e: LogEvent) =
  let originalColor = System.Console.ForegroundColor
  try
    System.Console.ForegroundColor <- consoleColor e.level
    System.Console.WriteLine (pprintLogEvent e)
  finally
    System.Console.ForegroundColor <- originalColor

