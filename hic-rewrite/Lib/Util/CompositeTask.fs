﻿module Twc.Util.Task.CompositeTask

open Twc.Util.Task


type t () =
  let members = ref []

  member x.Add (c: ITask) = members := c :: (!members)
  member x.Start () = !members |> List.iter (fun c -> c.Start ())
  member x.Terminate () = !members |> List.iter (fun c -> c.Terminate ())
  
  interface ITask with
    member x.Start () = x.Start ()
    member x.Terminate () = x.Terminate ()
    
     
let create () = new t ()

