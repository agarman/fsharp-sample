﻿namespace Twc.Util.Logging


type Message =
  | Simple of obj
  | WithException of obj * System.Exception
  | WithType of obj * System.Type
  | Complex of obj * Message


type Level = 
  | FATAL
  | WARN
  | ERROR
  | INFO
  | DEBUG

  
type LogEvent = {
  level : Level
  message : Message
  }

