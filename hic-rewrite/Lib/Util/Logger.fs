﻿module Twc.Util.Logging.Logger


open Twc.Util.Logging


// LogHandlers / Appenders
let private LogHandlers = new Event<LogEvent> ()


// Dispatch to logHandlers
let private Enqueue e = 
  async { LogHandlers.Trigger e }
  |> Async.Start


// Public interface
let private Info m = Enqueue { level = INFO; message = m }
let private Warn m = Enqueue { level = WARN; message = m }
let private Error m = Enqueue { level = ERROR; message = m }
let private Debug m = Enqueue { level = DEBUG; message = m }


type Handler =
  static member Register f =
    let f e = lock f (fun () -> f e)
    Event.add f LogHandlers.Publish

  static member Register (f, l) = 
    let f e = lock f (fun () -> if e.level = l then f e)
    Event.add f LogHandlers.Publish

  static member Register (f, (typ:System.Type)) =
    let rec hasType m =
      match m with
      | WithType (_, t) -> t = typ
      | Complex (_, m) -> hasType m
      | _ -> false

    let f e = lock f (fun () -> if hasType e.message then f e)
    Event.add f LogHandlers.Publish


/// This is the log facility.
type Log =
  static member Info fmt = Printf.ksprintf (fun s -> Info (Simple s)) fmt
  static member Info (o, e) = Info (WithException (o, e))

  static member Warn fmt = Printf.ksprintf (fun s -> Warn (Simple s)) fmt
  static member Warn (o, e) = Warn (WithException (o, e))

  static member Error fmt = Printf.ksprintf (fun s -> Error (Simple s)) fmt
  static member Error (o, e) = Error (WithException (o, e)) 

  static member Debug fmt = Printf.ksprintf (fun s -> Debug (Simple s)) fmt
  static member Debug (o, e) = Debug (WithException (o, e))

