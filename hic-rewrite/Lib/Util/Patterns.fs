﻿module Twc.Util.Patterns


let (|AsInt|_|) inp =
  let res = ref 0
  match System.Int32.TryParse (inp, res) with
  | true -> Some(!res)
  |_ -> None

let (|Match|_|) pattern input =
  let m = System.Text.RegularExpressions.Regex.Match(input, pattern) in
  if m.Success then Some (List.tail [ for g in m.Groups -> g.Value ]) else None

let (|Url|_|) inp =
  match inp with
  | Match @"^(.*)://(.*):(.*)$" [urn; host; AsInt port] -> Some(urn, host, port)
  | _ -> None

