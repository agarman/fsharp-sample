﻿using System;
using System.Runtime.Remoting;
using PlayList;

namespace Twc.Hic.Base.CorbaBridge
{
	public interface IPlaylistServerDelegate
	{
		void Attach(string observerName, Observer obs, short size, string[] idFilterList, out int id);

		void Detach(int id);

		void Ping();
	}


	public class PlayListSubjectImpl : MarshalByRefObject, Subject
	{
		public PlayListSubjectImpl(IPlaylistServerDelegate d)
		{
			Delegate = d;
		}

		public IPlaylistServerDelegate Delegate { get; set; }

		#region Subject Members

		public void attach(string observerName, Observer obs, short size, string[] idFilterList, out int id)
		{
			id = 0;
			try
			{
				Delegate.Attach(observerName, obs, size, idFilterList, out id);
			}
			catch (Exception ex)
			{
				Console.WriteLine("unhandled exception in IPlaylistServerDelegate: {0}", ex.Message);
			}
		}

		public void detach(int id)
		{
			try
			{
				Delegate.Detach(id);
			}
			catch (Exception ex)
			{
				Console.WriteLine("unhandled exception in IPlaylistServerDelegate: {0}", ex.Message);
			}
		}

		public void ping()
		{
			try
			{
				Delegate.Ping();
			}
			catch (Exception ex)
			{
				Console.WriteLine("unhandled exception in IPlaylistServerDelegate: {0}", ex.Message);
			}
		}

		#endregion

		public static PlayListSubjectImpl StartServer(string uri, IPlaylistServerDelegate d)
		{
			var impl = new PlayListSubjectImpl(d);
			RemotingServices.Marshal(impl, uri);
			return impl;
		}

		public override object InitializeLifetimeService()
		{
			return null;
		}
	}
}