﻿using System;
using System.Collections.Generic;
using System.Runtime.Remoting.Channels;
using Ch.Elca.Iiop;
using Ch.Elca.Iiop.Idl;
using Ch.Elca.Iiop.Services;
using omg.org.CosNaming;

namespace Twc.Hic.Base.CorbaBridge
{
	public interface IPlaylistClientDelegate
	{
		void SetItems(PlayList.Item[] items);
		void Update(PlayList.Observer_package.Notification[] notifications);
	}

	[SupportedInterface(typeof(PlayList.Observer))]
	public class PlaylistObserver : MarshalByRefObject, PlayList.Observer
	{
		public IPlaylistClientDelegate Delegate { get; set; }

		public PlayList.Subject PlaylistServer { get; set; }

		public int ClientIdentifier { get; set; }
		
		public static PlaylistObserver StartClient(
			string nameServiceHost, 
			int nameServicePort, 
			string serverUri,
			string clientName, 
			short windowSize, 
			string[] idFilterList, 
			IPlaylistClientDelegate del)
		{
			// lookup server in CORBA name service
			CorbaInit init = CorbaInit.GetInit();
			NamingContext nameService = init.GetNameService(nameServiceHost, nameServicePort);
			NameComponent[] name = new NameComponent[] { new NameComponent(serverUri, "") };
			object obj = nameService.resolve(name);
			PlayList.Subject pl = (PlayList.Subject)obj;

			PlaylistObserver observer = new PlaylistObserver
			{
				PlaylistServer = pl,
				Delegate = del
			};

			int id;
			pl.attach(clientName, observer, windowSize, idFilterList, out id);
			observer.ClientIdentifier = id;

			return observer;
		}

		public override object InitializeLifetimeService()
		{
			return null;
		}

		public void setItems(PlayList.Item[] items)
		{
			try
			{
				Delegate.SetItems(items);
			}
			catch (Exception ex)
			{
				Console.WriteLine("unhandled exception in IPlaylistClientDelegate: {0}", ex.Message);
			}
		}

		public void update(PlayList.Observer_package.Notification[] notifications)
		{
			try
			{
				Delegate.Update(notifications);
			}
			catch (Exception ex)
			{
				Console.WriteLine("unhandled exception in IPlaylistClientDelegate: {0}", ex.Message);
			}
		}
	}

}
