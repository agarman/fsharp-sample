﻿namespace Twc.Hic.PlaylistServerInterface
{
	public enum ItemType : byte
	{
		AudioVideo = 0,

		Audio = 1,

		Video = 2,

		Break = 16,

		Logo = 32,

		BreakSync = 33,

		SecAudioVideo = 128,

		SecAudio = 129,

		SecVideo = 130,

		SecKey = 131,

		SecTransKey = 132,

		SecBackTimedAudioVideo = 133,

		SecBackTimedGPI = 136,

		SecGPI = 137,

		SecTransAudioOver = 144,

		SecAudioOver = 145,

		SecData = 160,

		SecSystem = 164,

		SecBackSystem = 165,

		SecRecordSwitch = 176,

		SecSourceSwitch = 177,

		SecRecordDevice = 181,

		SecComment = 224,

		SecCompileID = 225,

		SecAppFlag = 226,

		SecBarterSpot = 227,

		Invalid = 255,
	}
}