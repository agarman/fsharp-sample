﻿namespace Twc.Hic.PlaylistServerInterface
{
	public struct TimeValue
	{
		public int Hour;

		public int Minute;

		public int Second;

		public int Frame;

		public TimeValue(byte hour, byte minute, byte second, byte frame)
		{
			Hour = hour;
			Minute = minute;
			Second = second;
			Frame = frame;
		}
	}
}