﻿using System.ServiceModel;

namespace Twc.Hic.PlaylistServerInterface
{
	public interface IPlaylistSubscriber
	{
		[OperationContract(IsOneWay = true)]
		void SetItems(PlaylistItem[] playlist);

		[OperationContract(IsOneWay = true)]
		void Update(PlaylistUpdateMessage[] updates);
	}


	[ServiceContract(Namespace = "Twc.Hic",
		Name = "OutboundClient",
		CallbackContract = typeof(IPlaylistServer))]
	public interface IPlaylistSubscriberListener
	{
		[OperationContract(IsOneWay = true)]
		void ConnectTo(int port, string withName);
	}


	[ServiceContract(Namespace = "Twc.Hic",
		Name = "PlaylistServer",
		CallbackContract = typeof(IPlaylistSubscriber))]
	public interface IPlaylistServer
	{
		[OperationContract]
		void Ping();

		[OperationContract]
		string Subscribe(string name, int windowSize, string[] filterList);

		[OperationContract(IsOneWay = true)]
		void Unsubscribe(string subscriberId);
	}


	[ServiceContract(Namespace = "Twc.Hic", Name = "OutboundService")]
	public interface IPlaylistServerOutbound
	{
		[OperationContract(IsOneWay = true)]
		void ConnectTo(string host, int port);
	}
}