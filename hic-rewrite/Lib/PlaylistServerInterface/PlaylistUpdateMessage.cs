﻿using System.Runtime.Serialization;
using System.ServiceModel;

namespace Twc.Hic.PlaylistServerInterface
{
	[MessageContract]
	[KnownType(typeof (PlaylistAddMessage))]
	[KnownType(typeof (PlaylistDeleteMessage))]
	[KnownType(typeof (PlaylistModifyMessage))]
	public class PlaylistUpdateMessage
	{
		[MessageHeader] public int Index;

		public PlaylistUpdateMessage()
		{
		}

		public PlaylistUpdateMessage(int idx)
		{
			Index = idx;
		}
	}


	[MessageContract]
	public class PlaylistAddMessage : PlaylistUpdateMessage
	{
		[MessageBodyMember] public PlaylistItem Item;

		public PlaylistAddMessage()
		{
		}

		public PlaylistAddMessage(int idx, PlaylistItem item)
			: base(idx)
		{
			Item = item;
		}
	}


	[MessageContract]
	public class PlaylistDeleteMessage : PlaylistUpdateMessage
	{
		public PlaylistDeleteMessage()
		{
		}

		public PlaylistDeleteMessage(int idx) : base(idx)
		{
		}
	}


	[MessageContract]
	public class PlaylistModifyMessage : PlaylistUpdateMessage
	{
		[MessageBodyMember] public PlaylistItem Item;

		public PlaylistModifyMessage()
		{
		}

		public PlaylistModifyMessage(int idx, PlaylistItem item)
			: base(idx)
		{
			Item = item;
		}
	}
}