﻿using System;

namespace Twc.Hic.PlaylistServerInterface
{
	public class PlaylistItem
	{
		public ItemAudioType AudioType;

		public bool AutoContactStart;

		public bool AutoDeadroll;

		public bool AutoException;

		public bool AutoMarkTime;

		public bool AutoPlay;

		public bool AutoRecord;

		public bool AutoSwitch;

		public bool AutoThread;

		public bool AutoTimed;

		public bool AutoUpCount;

		public byte BinHigh;

		public byte BinLow;

		public ItemChannel Channel;

		public byte DeviceMajor;

		public byte DeviceMinor;

		public bool Done;

		public TimeValue Duration;

		public bool IdTitle;

		public bool ManualStart;

		public bool NotPlayed;

		public bool NotSwitched;

		public TimeValue OnAirTime;

		public bool PlayedNextVideo;

		public bool Postrolled;

		public bool Preped;

		public bool Prerolled;

		public bool Previewed;

		public bool RanShort;

		public bool RollingNext;

		public bool RunLong;

		public bool RunShort;
		public bool Running;
		public TimeValue SOM;

		public bool Secondary;

		public short Segment;

		public bool Skipped;

		public bool StandByOn;

		public bool SwitchAudioOnly;

		public bool SwitchAudioVideoIndependent;

		public bool SwitchRejoin;

		public bool SwitchVideoOnly;

		public ItemType Type;

		public bool UserBitOnly;
		public string Id { get; set; }
		public string ReconciliationKey { get; set; }
		public string Title { get; set; }

		public ItemType ItemType
		{
			get { return Type; }
			set { Type = value; }
		}

		public TimeValue StartTime { get; set; }

		public string StartTimeDisplay
		{
			get { return FormatVideoTimeCode(StartTime); }
		}

		public string DurationDisplay
		{
			get { return FormatVideoTimeCode(Duration); }
		}

		private static string FormatVideoTimeCode(TimeValue tc)
		{
			return String.Format("{0}:{1}:{2}:{3}", tc.Hour, tc.Minute, tc.Second, tc.Frame);
		}
	}
}