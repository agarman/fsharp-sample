﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Twc.Hic.PlaylistServerInterface
{
	public class SubscriberInfo
	{
		public SubscriberStatus Status;
		public DateTime LastContact;

		public DateTime ConnectedOn;
		public string ConnectionType;

		public string Name;
		public int WindowSize;
		public string[] Filters;

		public string[] ConnectionInfo;

		public override string ToString()
		{
			return String.Format("Connected via {0} on {1} with Size: {2} and Filters: {3}",
				ConnectionType, ConnectedOn, WindowSize, String.Join(";", Filters));
		}
	}

	public enum SubscriberStatus
	{
		Connecting,
		SentItems,
		Updating,
		Disconnected,
		Error,
	}
}