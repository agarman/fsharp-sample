﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace Twc.Hic.PlaylistServerInterface
{
	[ServiceContract(Name="ServiceInfo")]
	public interface IServiceInfo
	{
		[OperationContract]
		SubscriberInfo[] Connections();

		[OperationContract]
		SubscriberInfo[] OutboundClients();

		[OperationContract]
		SourceInfo Source();

		[OperationContract]
		PlaylistItem[] Playlist(int windowSize, string[] filters);

		[OperationContract]
		PlaylistItem[] ClientsPlaylist(string clientName);
	}
}