﻿namespace Twc.Hic.PlaylistServerInterface
{
	public enum ItemAudioType : byte
	{
		Stereo = 0,

		Mono = 1,

		MonoLeft = 2,

		MonoRight = 3,

		NotApplicable = 4
	}
}