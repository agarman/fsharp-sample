﻿module GuiViewer

open System
open System.Windows
open System.Windows.Controls


let create closeFn =
  // We have to create a thread to run the window.  The window must be created on the same
  // thread that created it.  Therfore, we have to spawn the thread and let it create the
  // window and the function that will update the window.  At this point, the observer can
  // be generated that will invoke the window update function.

  let callback = ref (fun (p : Playlist.playlist) -> ())
  let update pl = (!callback) pl
  let are = new System.Threading.AutoResetEvent (false)

  let doWindow () =
    let baml = "/Twc.Hic.GuiElements;component/mainwindow.xaml"
    let uri = new Uri (baml, UriKind.Relative)
    let window = Application.LoadComponent(uri) :?> Window
    window.Closed.Add (fun _ -> closeFn ())

    let listView = window.FindName "ListView" :?> ListView

    let updateListview (pl: Playlist.playlist) =
      ignore <| listView.Dispatcher.Invoke (new Action (fun () ->
        listView.Items.Clear ()
        Seq.iter (fun it -> listView.Items.Add it |> ignore) (Playlist.toItems pl)))

    callback := updateListview
    are.Set () |> ignore

    let app = new Application ()
    app.Run window |> ignore

  let t = new System.Threading.Thread (new System.Threading.ThreadStart (doWindow))
  t.SetApartmentState (System.Threading.ApartmentState.STA)
  t.Start ()

  are.WaitOne () |> ignore
  Observer.makeObserver (Observer.withPlaylist update)

