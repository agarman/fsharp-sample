﻿module Hic

open Ch.Elca.Iiop
open System.Runtime.Remoting.Channels
open Twc.Util.Logging.Logger
open Twc.Util
open Twc.Util.Patterns


let usage =
  @"
  Usage: 
    hic.exe mode source [window] [filters]

  Mode:
    server   - starts the Playlist service (Wcf:55757, Corba:8087)
    viewer   - starts a command line monitor of Playlist
    gui      - starts a gui viewer of Playlist
    playlist - retrieves the current Playlist

  Source:
    simulator            - generate a fake playlist for testing purposes
    wcf://host:port      - connects to a Playlist server via Wcf
    corba://host:port    - connects to a Playlist server via Corba
    listener://host:port - start a client listener

  Window:
    Maximum number of elements to return

  Filters:
    A white space deliminated list Event filters (e.g., IXL, ISTAR, ALL)

  Examples:
    hic.exe server simulator
      Starts server with a generated Playlist for testing

    hic.exe viewer wcf://localhost:55757 IXL ISTAR
      Starts a console viewer connecting to net.tcp://localhost:55757
      Limit results to IXL and ISTAR events

    hic.exe playlist corba://remotehost1:4000 300 IXL ISTAR
      Retrieves current playlist from remotehost.
      Limit results to 300 events.
      Limit results to IXL and ISTAR events

  Advanced Usage:
    hic.exe connect wcf://ServerHost:port wcf://ClientHost:port
      Commands Playlist server ServerHost:port to connect to ClientHost:port

    hic.exe clients wcf://host:port
      lists information about clients connect to server"
  |> printfn "%s"


let decorateWithPlaylistPrinter =
  Observer.decorate (Util.asSideEffect (Playlist.pprintUpdate >> printfn "%s\n")) >>
  Observer.decorate (Observer.withPlaylist (Playlist.pprintPlaylist >> printfn "%s\n"))


let decorateWithMonitoring connType =
  ClientMonitor.monitor connType "source" System.Int32.MaxValue [||]


let are = new System.Threading.AutoResetEvent (false)

let shutdown () = are.Set () |> ignore

let consoleTask (are: System.Threading.AutoResetEvent) =
  let signalShutdown () = are.Set () |> ignore

  System.Console.TreatControlCAsInput <- false
  Log.Info "Press <CTRL-C> to exit.\n\n"

  { new Task.ITask with
    member x.Start () = System.Console.CancelKeyPress.Add (fun e -> e.Cancel <- true; shutdown ())
    member x.Terminate () = ()
  }


[<System.STAThread>]
do
  // This match reads through the arguments to assure that the correct number have been passed.
  let args = System.Environment.GetCommandLineArgs () |> Seq.toList

  match args with
  | _ :: [] | _ :: "usage" :: _ -> usage

  | _ :: "connect"  :: Url ("wcf", h, p) :: Url ("wcf", ch, cp) :: [] ->
    WcfClientListener.connect h p ch cp

  | _ :: "clients"  :: Url ("wcf", h, p) :: cmd                       ->
    MonitoringClient.info h p cmd |> Seq.iter(printfn "%A")

  | _ :: "playlist" ::  Url ("wcf", h, p) :: cmd                      ->
    MonitoringClient.playlist h p cmd |> Seq.iter(Playlist.pprintItem >> printfn "%A")

  | _ ->
    Handler.Register Twc.Util.Logging.LogHandlers.console
    Log.Info "Starting...\n"

    let mode, url, wSize, filters =
      match args with
      | _ :: mode :: url :: AsInt wSize :: filters -> mode, url, wSize, filters
      | _ :: mode :: url :: filters                -> mode, url, 100,   filters
      | _ -> usage; failwith "Invalid arguments"

    let task =
      // Here we create the appropriate IPlaylistObserver based on the mode.
      match mode with
      | "server" -> Observer.compositeObserver [WcfServer.create 55757; CorbaServer.create 8087; MonitoringService.create 55757]
      | "viewer" -> decorateWithPlaylistPrinter Observer.sink
      | "gui"    -> GuiViewer.create shutdown
      | _        -> usage; failwithf "Unsupported mode: %s" mode 

      |> // Feeding the result into a match that creates a task

      match url with
      | "simulator"                  -> (decorateWithMonitoring "simulator") >> PlaylistSimulator.create
      | Url ("listener", host, port) -> (decorateWithMonitoring "listener")  >> WcfClientListener.create port          wSize filters 5000
      | Url ("wcf",      host, port) -> (decorateWithMonitoring "wcf")       >> WcfClient.create    host port "source" wSize filters 5000
      | Url ("corba",    host, port) -> (decorateWithMonitoring "corba")     >> CorbaClient.create  host port "source" wSize filters 5000
      | _                            -> usage; failwithf "Unsupported URL: %s\n" url

    let masterTask = Task.CompositeTask.create ()
    masterTask.Add (consoleTask are)
    masterTask.Add task

    // At this point, we have a client wired back through to our IPlaylistObserver
    Log.Info "Initing...\n"
    masterTask.Start ()
    Log.Info "Running...\n"

    are.WaitOne () |> ignore
    Log.Info "Exiting.\n"

    masterTask.Terminate ()

