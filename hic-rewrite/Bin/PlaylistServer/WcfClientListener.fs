﻿module WcfClientListener

open System.ServiceModel
open System.Threading.Tasks
open Twc.Hic.PlaylistServerInterface


[<ServiceBehavior(InstanceContextMode=InstanceContextMode.Single)>]
type private server (wSize:int, filters:string seq, wDogM:int, observer:Observer.IPlaylistObserver) =
  let connected = ref false
  let terminate = ref (fun () -> ())

  member x.Terminate () = 
    (!terminate) ()
    connected := false

  interface IPlaylistSubscriberListener with
    member x.ConnectTo (port, name) =
      match !connected with
      | false ->

          let remoteProps = OperationContext.Current.IncomingMessageProperties
          let endPt = remoteProps.[Channels.RemoteEndpointMessageProperty.Name] :?> Channels.RemoteEndpointMessageProperty
          let host = if endPt.Address = "::1" then "localhost" else endPt.Address

          let task = WcfClient.create host port name wSize filters wDogM observer

          terminate := (fun () -> task.Terminate())
          task.Start()
          connected := true

      | true -> ()


let create port wSize filters wDogM (observer: Observer.IPlaylistObserver) =
  let url = sprintf "net.tcp://localhost:%d/Twc/Hic/OutboundClient" port
  let uri = new System.Uri (url)
  let svr = new server (wSize, filters, wDogM, observer)
  let host = new ServiceHost(svr, uri)

  { new Twc.Util.Task.ITask with
    member x.Start () =
      host.Open()

    member x.Terminate () =
      svr.Terminate()
  }


let connect server localport host port =
  let addr = sprintf "net.tcp://%s:%d/Twc/Hic/PlaylistServer" server localport
  let endp = new EndpointAddress (addr)
  let factory = new ChannelFactory<IPlaylistServerOutbound> (new NetTcpBinding (), endp)
  let svr = factory.CreateChannel()
  svr.ConnectTo(host, port)