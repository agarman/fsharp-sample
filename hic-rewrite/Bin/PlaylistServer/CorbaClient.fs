﻿module CorbaClient

open Twc.Hic.Base


let create host port name wSize filters watchdogMilli (observer: Observer.IPlaylistObserver) =
  let props = new System.Collections.Generic.Dictionary<string, string>()
  let rand = new System.Random ()
  props.Add(Ch.Elca.Iiop.IiopServerChannel.PORT_KEY, string (rand.Next (5000, 8000)))
  props.Add("name", "client")

  let channel = new Ch.Elca.Iiop.IiopChannel (props)
  System.Runtime.Remoting.Channels.ChannelServices.RegisterChannel (channel, false)

  let (obs : CorbaBridge.PlaylistObserver ref) = ref null in
    let connect () =
      let cc = 
        { new Twc.Hic.Base.CorbaBridge.IPlaylistClientDelegate with
              member x.SetItems items = lock x (fun () ->
                Seq.map Corba.itemOfCorbaItem items
                |> Seq.toList
                |> Playlist.SetItems 
                |> observer.Update)

              member x.Update upds = lock x (fun () ->
                Seq.map Corba.itemUpdateOfNotification upds
                |> Seq.toList
                |> Playlist.UpdateItems
                |> observer.Update)
        }

      obs := Twc.Hic.Base.CorbaBridge.PlaylistObserver.StartClient(host, port, "PlayListSubject", name, int16 wSize, Array.ofList filters, cc)
      (!obs).ClientIdentifier

    let disconnect id =
      (!obs).PlaylistServer.detach id

    let ping () =
      (!obs).PlaylistServer.ping ()

  ConnectionMgr.create<int> watchdogMilli connect disconnect ping