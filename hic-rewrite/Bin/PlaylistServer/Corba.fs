﻿module Corba

open PlayList.Observer_package

let fromTime (tv : Twc.Hic.PlaylistServerInterface.TimeValue) = 
  let mutable tv' = PlayList.TimeValue ()
  tv'.hour   <- int16 tv.Hour
  tv'.minute <- int16 tv.Minute
  tv'.second <- int16 tv.Second
  tv'.frame  <- int16 tv.Frame
  tv'


let toTime (tv : PlayList.TimeValue) = 
  let mutable tv' = Twc.Hic.PlaylistServerInterface.TimeValue ()
  tv'.Hour   <- int tv.hour
  tv'.Minute <- int tv.minute
  tv'.Second <- int tv.second
  tv'.Frame  <- int tv.frame
  tv'


let corbaItemOfItem (i: Playlist.item) =
  let mutable foo = new PlayList.Item ()
  foo.SOM                         <- fromTime i.SOM
  foo.audioType                   <- LanguagePrimitives.EnumOfValue (int i.AudioType) 
  foo.autoContactStart            <- i.AutoContactStart
  foo.autoDeadroll                <- i.AutoDeadroll
  foo.autoException               <- i.AutoException
  foo.autoMarkTime                <- i.AutoMarkTime
  foo.autoPlay                    <- i.AutoPlay
  foo.autoRecord                  <- i.AutoRecord
  foo.autoSwitch                  <- i.AutoSwitch
  foo.autoThread                  <- i.AutoThread
  foo.autoTimed                   <- i.AutoTimed
  foo.autoUpCount                 <- i.AutoUpCount
  foo.binHigh                     <- i.BinHigh
  foo.binLow                      <- i.BinLow
  foo.channel                     <- LanguagePrimitives.EnumOfValue (int i.Channel)
  foo.``type``                    <- LanguagePrimitives.EnumOfValue (int i.Type)
  foo.deviceMajor                 <- i.DeviceMajor
  foo.deviceMinor                 <- i.DeviceMinor
  foo.``done``                    <- i.Done  
  foo.duration                    <- fromTime i.Duration
  foo.id                          <- i.Id 
  foo.idTitle                     <- i.IdTitle
  foo.manualStart                 <- i.ManualStart
  foo.notPlayed                   <- i.NotPlayed
  foo.notSwitched                 <- i.NotSwitched
  foo.onAirTime                   <- fromTime i.OnAirTime
  foo.playedNextVideo             <- i.PlayedNextVideo
  foo.postrolled                  <- i.Postrolled
  foo.preped                      <- i.Preped
  foo.prerolled                   <- i.Prerolled
  foo.previewed                   <- i.Previewed
  foo.ranShort                    <- i.RanShort
  foo.reconciliationKey           <- i.ReconciliationKey
  foo.rollingNext                 <- i.RollingNext
  foo.runLong                     <- i.RunLong
  foo.runShort                    <- i.RunShort
  foo.running                     <- i.Running
  foo.secondary                   <- i.Secondary
  foo.segment                     <- i.Segment
  foo.skipped                     <- i.Skipped
  foo.standByOn                   <- i.StandByOn
  foo.switchAudioOnly             <- i.SwitchAudioOnly
  foo.switchAudioVideoIndependent <- i.SwitchAudioVideoIndependent
  foo.switchRejoin                <- i.SwitchRejoin
  foo.switchVideoOnly             <- i.SwitchVideoOnly
  foo.title                       <- i.Title
  foo.userBitOnly                 <- i.UserBitOnly
  foo


let itemOfCorbaItem (i : PlayList.Item) =
  let foo = new Playlist.item ()
  foo.SOM                         <- toTime i.SOM
  foo.AudioType                   <- LanguagePrimitives.EnumOfValue (byte i.audioType) 
  foo.AutoContactStart            <- i.autoContactStart
  foo.AutoDeadroll                <- i.autoDeadroll
  foo.AutoException               <- i.autoException
  foo.AutoMarkTime                <- i.autoMarkTime
  foo.AutoPlay                    <- i.autoPlay
  foo.AutoRecord                  <- i.autoRecord
  foo.AutoSwitch                  <- i.autoSwitch
  foo.AutoThread                  <- i.autoThread
  foo.AutoTimed                   <- i.autoTimed
  foo.AutoUpCount                 <- i.autoUpCount
  foo.BinHigh                     <- i.binHigh
  foo.BinLow                      <- i.binLow
  foo.Channel                     <- LanguagePrimitives.EnumOfValue (byte i.channel)
  foo.Type                        <- LanguagePrimitives.EnumOfValue (byte i.``type``)
  foo.DeviceMajor                 <- i.deviceMajor
  foo.DeviceMinor                 <- i.deviceMinor
  foo.Done                        <- i.``done``  
  foo.Duration                    <- toTime i.duration
  foo.Id                          <- i.id 
  foo.IdTitle                     <- i.idTitle
  foo.ManualStart                 <- i.manualStart
  foo.NotPlayed                   <- i.notPlayed
  foo.NotSwitched                 <- i.notSwitched
  foo.OnAirTime                   <- toTime i.onAirTime
  foo.PlayedNextVideo             <- i.playedNextVideo
  foo.Postrolled                  <- i.postrolled
  foo.Preped                      <- i.preped
  foo.Prerolled                   <- i.prerolled
  foo.Previewed                   <- i.previewed
  foo.RanShort                    <- i.ranShort
  foo.ReconciliationKey           <- i.reconciliationKey
  foo.RollingNext                 <- i.rollingNext
  foo.RunLong                     <- i.runLong
  foo.RunShort                    <- i.runShort
  foo.Running                     <- i.running
  foo.Secondary                   <- i.secondary
  foo.Segment                     <- i.segment
  foo.Skipped                     <- i.skipped
  foo.StandByOn                   <- i.standByOn
  foo.SwitchAudioOnly             <- i.switchAudioOnly
  foo.SwitchAudioVideoIndependent <- i.switchAudioVideoIndependent
  foo.SwitchRejoin                <- i.switchRejoin
  foo.SwitchVideoOnly             <- i.switchVideoOnly
  foo.Title                       <- i.title
  foo.UserBitOnly                 <- i.userBitOnly
  foo


let itemListOfUpdate (pl : Playlist.item list) =
  pl |> Seq.map (corbaItemOfItem) |> Seq.toArray


let corbaItemOpt i =
  let opt = new ItemOpt ()
  opt.SetDefault false

  if box i <> null then opt.Setitm (corbaItemOfItem i)
  
  opt


let notificationOfItemUpdate (iu: Playlist.itemUpdate) =
  match iu with
  | Playlist.Add (idx, itm)    -> new Notification (int16 idx, NotificationType.Add, corbaItemOpt itm)
  | Playlist.Delete (idx)      -> new Notification (int16 idx, NotificationType.Delete, corbaItemOpt null)
  | Playlist.Modify (idx, itm) -> new Notification (int16 idx, NotificationType.Modify, corbaItemOpt itm)


let itemUpdateOfNotification (n: Notification) =
  match n.``type`` with
  | NotificationType.Add    -> Playlist.Add (int n.index, (itemOfCorbaItem (n.itmOpt.Getitm ())))
  | NotificationType.Delete -> Playlist.Delete (int n.index)
  | NotificationType.Modify -> Playlist.Modify (int n.index, (itemOfCorbaItem (n.itmOpt.Getitm ())))
  | _ -> failwithf "unknown Corba notifaction type: %A" n

