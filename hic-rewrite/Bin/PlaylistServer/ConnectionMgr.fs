﻿module ConnectionMgr

open Twc.Util.Logging.Logger


let create<'a when 'a : equality> watchdogMilli connectFn disconnectFn pingFn =
  let clientId = ref Unchecked.defaultof<'a>

  let dosafe fn =
    Twc.Util.Util.dosafe fn (fun ex ->
      Log.Error "communication with server failed; resetting connection:\n%s" ex.Message
      clientId := Unchecked.defaultof<'a>)

  let connect () =
    Log.Info "connecting to server..."
    clientId := connectFn ()
    Log.Info "server connection established"

  let disconnect () =
    Log.Info "disconnecting from server..."
    let id = !clientId
    clientId := Unchecked.defaultof<'a>
    disconnectFn id

  let ping () = pingFn ()

  let watchdog _ =
    let cid = !clientId
    if cid = Unchecked.defaultof<'a> then 
      connect ()
    else 
      dosafe (ping)

  let watchdogTimer = new System.Threading.Timer (new System.Threading.TimerCallback (watchdog))

  { new Twc.Util.Task.ITask with
    member x.Start () =
      dosafe connect
      watchdogTimer.Change ((watchdogMilli: int), watchdogMilli) |> ignore

    member x.Terminate () =
      let infinity = System.Threading.Timeout.Infinite
      watchdogTimer.Change (infinity, infinity) |> ignore    
      dosafe disconnect
  }

