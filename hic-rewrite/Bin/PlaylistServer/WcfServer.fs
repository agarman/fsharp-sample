﻿module WcfServer

open System.ServiceModel
open Twc.Hic


[<ServiceBehavior(InstanceContextMode=InstanceContextMode.Single)>]
type private server (mport) =
  let publisher, attach, detach, onError = MonitoredPublisher.create "wcf"

  member private x.Factory =
    new DuplexChannelFactory<PlaylistServerInterface.IPlaylistSubscriberListener> (x, new NetTcpBinding())

  interface Observer.IPlaylistObserver with
    member x.Update upd =  lock publisher (fun () -> publisher.Update upd)

  interface Twc.Hic.PlaylistServerInterface.IPlaylistServerOutbound with
    member x.ConnectTo (host:string, port:int) =
      try
        let addr = sprintf "net.tcp://%s:%d/Twc/Hic/OutboundClient" host port
        let endp = new EndpointAddress (addr)
        let svr = x.Factory.CreateChannel(endp)
        svr.ConnectTo (mport, "outbound")
      with ex -> ()

  interface Twc.Hic.PlaylistServerInterface.IPlaylistServer with
    member x.Ping () = ()

    member x.Subscribe (name, wSize, filters) =
      let id = sprintf "%s:%A" name (System.Guid.NewGuid())

      let proxy =
        OperationContext.Current.GetCallbackChannel<PlaylistServerInterface.IPlaylistSubscriber>()

        |> (fun o ->
              ClientProxy.create id wSize filters
                (Seq.toArray >> o.SetItems)
                (Seq.map (Playlist.updateMsgOfItemUpdate) >> Seq.toArray >> o.Update)
                onError)

      lock publisher (fun () -> attach id wSize filters proxy)

      id

    member x.Unsubscribe id = detach id


/// Starts a Wcf Service and returns an observer
let create port =
  let url = sprintf "net.tcp://localhost:%d/Twc/Hic/PlaylistServer" port
  let uri = new System.Uri (url)
  let svr = new server (port)
  let host = new ServiceHost (svr, uri)

  host.Open ()
  svr :> Observer.IPlaylistObserver

