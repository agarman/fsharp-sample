﻿module MonitoringClient

open System.ServiceModel
open Twc.Hic
open Twc.Util.Patterns


let private Factory = new ChannelFactory<PlaylistServerInterface.IServiceInfo>(new NetTcpBinding())

let private server host port =
  let addr = sprintf "net.tcp://%s:%d/Twc/Hic/Monitor" host port
  let endp = new EndpointAddress(addr)
  Factory.CreateChannel(endp)


let info host port cmd =
  let svr = server host port
  match cmd with 
  | "connections" ::  [] -> svr.Connections()
  | "outbound" ::     [] -> svr.OutboundClients()
  | "source" ::       [] -> [| svr.Source().Source |]
  | "client" :: id :: [] -> svr.Connections() |> Seq.choose (fun y -> if y.Name = id then Some y else None) |> Seq.toArray
  | _                    -> failwithf "Unknown command"


let playlist host port cmd =
  let svr = server host port
  match cmd with
  | AsInt wSize :: filters     -> svr.Playlist (wSize, (filters |> Seq.toArray))
  | filters                    -> svr.Playlist (100,   (filters |> Seq.toArray))
  | _                          -> failwithf "Unknown command"
