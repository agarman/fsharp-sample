﻿module Pack

open Twc.Hic
open Twc.Util


let bytesOfString pad len s =
  let s = if (s = null) then "" else s (* robusty *)
  let res = (Array.create len pad)
  Util.blit (System.Text.Encoding.UTF8.GetBytes s) res 0
  res
  
let padWithFF = bytesOfString 0xFFuy
let padWithSpace = bytesOfString 0x20uy
let padWithNull = bytesOfString 0x00uy


let parseUntilTerminator c = Seq.takeWhile ((<>) c) >> Seq.toArray
let parseUntilFF = parseUntilTerminator 0xFFuy
let parseUntilSpace = parseUntilTerminator 0x20uy
let parseUntilNull = parseUntilTerminator 0x00uy


let timeValueOfArray (a: System.Byte[]) = new PlaylistServerInterface.TimeValue(a.[3], a.[2], a.[1], a.[0])
let bytesOfTimeValue (tv: PlaylistServerInterface.TimeValue) = 
  [| byte tv.Frame; byte tv.Second; byte tv.Minute; byte tv.Hour |]


/// Pulls data from Louth ADC100 Record byte stream into PlaylistItem
let unpackLouthItem (bytes: System.Byte[]) =
  let stringOfBytes = System.Text.Encoding.UTF8.GetString

  // NOTE: are any of these fields needed?
  //  let effect1             = bytes.[9]
  //  let effect2             = bytes.[10]
  //  let effect3             = bytes.[11]
  //  let qualifier4          = bytes.[49]
  //  let qualifier1          = bytes.[55]
  //  let qualifier2          = bytes.[56]
  //  let qualifier3          = bytes.[57]
  //  let dateToAir           = bytes.[58..59]
  //  let eventControl        = bytes.[60..61]
  //  let eventStatus         = bytes.[62..63]
  //  let compileId           = bytes.[64..71]
  //  let compileSom          = bytes.[72..75]
  //  let boxAId              = bytes.[76..83]
  //  let boxASom             = bytes.[84..87]
  //  let boxBId              = bytes.[88..95]
  //  let boxBSom             = bytes.[96..99]
  //  let reserved            = bytes.[100]
  //  let backupDeviceMajor   = bytes.[101]
  //  let backupDeviceMinor   = bytes.[102]
  //  let extendedEventCtrl   = bytes.[103]

  new PlaylistServerInterface.PlaylistItem (
    Type              = LanguagePrimitives.EnumOfValue bytes.[0],
    ReconciliationKey = (bytes.[1..8]   |> parseUntilFF |> stringOfBytes),
    Id                = (bytes.[16..23] |> parseUntilFF |> parseUntilSpace |> stringOfBytes),
    Title             = (bytes.[24..39] |> parseUntilSpace |> stringOfBytes),
    Channel           = LanguagePrimitives.EnumOfValue bytes.[48],
    BinHigh           = bytes.[53],
    BinLow            = bytes.[54],
    DeviceMajor       = bytes.[51],
    DeviceMinor       = bytes.[52],
    OnAirTime         = timeValueOfArray bytes.[12..15],
    SOM               = timeValueOfArray bytes.[40..43],
    Duration          = timeValueOfArray bytes.[44..47],
    Segment           = int16 bytes.[50]
  )


let packLouthItem (item: PlaylistServerInterface.PlaylistItem) =
  let bytes = Array.create 104 0uy
  let blit ar pos = Util.blit ar bytes pos

  // pos |> blit what-to-write
  0   |> blit [| byte item.ItemType |]
  1   |> blit (padWithFF      8 item.ReconciliationKey)
  16  |> blit (padWithFF      8 item.Id)
  24  |> blit (padWithSpace  16 item.Title)
  12  |> blit (bytesOfTimeValue item.OnAirTime)
  40  |> blit (bytesOfTimeValue item.SOM)
  44  |> blit (bytesOfTimeValue item.Duration)
  48  |> blit [| byte item.Channel |]
  50  |> blit [| byte item.Segment |]
  51  |> blit [| byte item.DeviceMajor |]
  52  |> blit [| byte item.DeviceMinor |]
  53  |> blit [| byte item.BinHigh |]
  54  |> blit [| byte item.BinLow |]

  bytes