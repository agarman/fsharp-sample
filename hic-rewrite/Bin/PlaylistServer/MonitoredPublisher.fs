﻿module MonitoredPublisher


open Twc.Util.Logging.Logger

/// Returns:
///   (Observer.IPlaylistObserver, attachFn, detachFn, onErrorFn)
let create connType =
  let publisher, attach, detach, onError = Publisher.create ()

  publisher
  ,
  (fun id wSize filters obs ->
    Log.Info "Client %A connected via %A" id connType
    ClientMonitor.monitor connType id wSize filters obs
    |> attach id)
    ,
  (fun id ->
    Log.Info "Client %A disconnected at %A" id System.DateTime.Now
    ClientMonitor.setStatus connType id Twc.Hic.PlaylistServerInterface.SubscriberStatus.Disconnected
    detach id)
    ,
  (fun id -> 
    Log.Info "Client %A connection failed at %A" id System.DateTime.Now
    ClientMonitor.setStatus connType id Twc.Hic.PlaylistServerInterface.SubscriberStatus.Error
    onError id)

