﻿module Publisher


/// Returns
/// (Observer.IPlaylistObserver, attachFn, detachFn, onErrorFn)
let create () = 
  let clients = ref Map.empty

  { new Observer.IPlaylistObserver with
      member x.Update upd =
        Map.iter (fun _ (c: Observer.IPlaylistObserver) -> c.Update upd) !clients
  }
  ,(fun id client -> clients := Map.add id client !clients)
  ,(fun id -> clients := Map.remove id !clients)
  ,(fun id -> clients := Map.remove id !clients)

