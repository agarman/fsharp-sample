﻿module MonitoringService


open Twc.Hic.PlaylistServerInterface
open System.ServiceModel


[<ServiceBehavior(InstanceContextMode=InstanceContextMode.Single)>]
type private server () =
  let playlist = ref Playlist.empty

  let getStatus name =
    ClientMonitor.list()
    |> Map.pick (fun (_,id) fn -> if id = name then Some(fn()) else None)

  let getPlaylist wSize filters =
    !playlist
    |> Playlist.sub wSize filters
    |> Playlist.toItems
    |> Seq.toArray

  interface IServiceInfo with
    member x.Connections () = ClientMonitor.clientStatus () |> Seq.toArray

    member x.Source () =
      new SourceInfo(Source = (getStatus "source"))

    member x.OutboundClients () =
      ClientMonitor.clientStatus ()
      |> Seq.filter (fun st -> st.Name.StartsWith("outbound"))
      |> Seq.toArray

    member x.Playlist (wSize:int, filters:string[]) =
      getPlaylist wSize filters

    member x.ClientsPlaylist (name:string) =
      let info = getStatus name
      getPlaylist info.WindowSize info.Filters

  interface Observer.IPlaylistObserver with
    member x.Update upd = playlist := Playlist.applyUpdate !playlist upd


let create port =
  let url = sprintf "net.tcp://localhost:%d/Twc/Hic/Monitor" port
  let uri = new System.Uri(url)
  let svr = new server ()
  let hst = new ServiceHost (svr, uri)
  hst.Open ()
  svr :> Observer.IPlaylistObserver

