﻿module Playlist

open Twc.Hic
open Twc.Util


type item = PlaylistServerInterface.PlaylistItem

type playlist = internal Playlist of list<item>

type itemPredicate = item -> bool

type itemUpdate =
  | Add of int * item
  | Delete of int
  | Modify of int * item

type update =
  | Nothing
  | SetItems of list<item>
  | UpdateItems of list<itemUpdate>


let empty = Playlist []


let item id = new item (ReconciliationKey = id)


/// if x item in xs doesn't exist in ys call fn (index of x in xs, x)
let chooseItems xs ys fn =
  let same (e1: item) (e2: item) = e1.ReconciliationKey = e2.ReconciliationKey
  let pred = (fun (i, e) -> if (Util.contains (same e) ys) then None else Some (fn i e))
  xs |> Seq.mapi (fun i e -> i, e)
  |> Seq.choose pred
  |> Seq.toList

/// if an item in prev is not in next, delete it
let deletes prev next = chooseItems prev next (fun i e -> Delete i)

/// if an item in next is not in prev, add it
let adds prev next =    chooseItems next prev (fun i e -> Add(i,e))


let updates prev next =
  (List.rev (deletes prev next)) @ (adds prev next)


let ofItems items = Playlist (Seq.toList items)


let toItems (Playlist pl) = pl :> seq<item>


let rec accumulate size src pred accum =
  if size <= 0 then accum else
  match src with
  | h::t  -> if pred h then h::accum else accum
             |> accumulate (size-1) t pred
  | []    -> accum


let sub size filters (Playlist pl) =
  let filterTest (e: item) = Util.has e.Id filters
  let test = if Seq.isEmpty filters then (Util.constant true) else filterTest
  Playlist (accumulate size pl test [])


let buildUpdate (Playlist prev) (Playlist next) =
  let us = updates prev next
  if us = [] then Nothing
  else if List.length us >= List.length next then SetItems next
  else UpdateItems us


let applyItemUpdate items itemUpd =
  match itemUpd with
  | Add (idx, item) ->
    let pre = Seq.take idx items
    let post = Seq.skip idx items
    Seq.toList (Seq.append pre (Seq.append [item] post))

  | Delete idx -> Seq.toList (Util.dropAt idx items)
 
  | Modify (idx, item) ->
    let pre = Seq.take idx items
    let post = Seq.skip (idx + 1) items
    Seq.toList (Seq.append pre (Seq.append [item] post))


let applyUpdate (pl: playlist) upd =
  let (Playlist items) = pl
  match upd with
  | Nothing          -> pl
  | SetItems items   -> Playlist items
  | UpdateItems iups -> Playlist (List.fold applyItemUpdate items iups)


let updateMsgOfItemUpdate (iu: itemUpdate) : PlaylistServerInterface.PlaylistUpdateMessage =
  match iu with
  | Add (idx, itm)    -> new PlaylistServerInterface.PlaylistAddMessage (idx, itm)    :> PlaylistServerInterface.PlaylistUpdateMessage
  | Delete (idx)      -> new PlaylistServerInterface.PlaylistDeleteMessage (idx)      :> PlaylistServerInterface.PlaylistUpdateMessage
  | Modify (idx, itm) -> new PlaylistServerInterface.PlaylistModifyMessage (idx, itm) :> PlaylistServerInterface.PlaylistUpdateMessage


let itemUpdateOfUpdateMsg (updMsg: PlaylistServerInterface.PlaylistUpdateMessage) =
  match updMsg with
  | :? PlaylistServerInterface.PlaylistAddMessage as m    -> Add (m.Index, m.Item)
  | :? PlaylistServerInterface.PlaylistDeleteMessage as m -> Delete m.Index
  | :? PlaylistServerInterface.PlaylistModifyMessage as m -> Modify (m.Index, m.Item)
  | _ -> failwithf "unknown update type: %A" updMsg


let updateOfUpdateMsg (upd: seq<PlaylistServerInterface.PlaylistUpdateMessage>) =
  UpdateItems (upd |> Seq.map itemUpdateOfUpdateMsg |> Seq.toList)


let pprintItem (e: item) =
  let st = e.StartTime
  sprintf "{%s %s %s %d:%d:%d:%d}" e.ReconciliationKey e.Id e.Title st.Hour st.Minute st.Second st.Frame


let pprintItemUpdate e =
  match e with
  | Add (i, e) -> sprintf "Add %d: %s" i (pprintItem e)
  | Delete i -> sprintf "Delete %d" i
  | Modify (i, e) -> sprintf "Modify %d: %s" i (pprintItem e)


let pprintUpdate e =
  match e with
  | Nothing         -> "Nothing"
  | SetItems items  -> "SetItems " + Util.pprintList "" "" "\n" pprintItem items
  | UpdateItems ups -> "Update " + Util.pprintList "" "" "\n" pprintItemUpdate ups


let pprintPlaylist (Playlist pl) =
  Util.pprintList "" "" "\n" pprintItem pl

