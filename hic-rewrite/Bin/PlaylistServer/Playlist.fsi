﻿module Playlist 


type item = Twc.Hic.PlaylistServerInterface.PlaylistItem

type playlist

type itemUpdate = 
  | Add of int * item
  | Delete of int
  | Modify of int * item

type update = 
  | Nothing 
  | SetItems of list<item>
  | UpdateItems of list<itemUpdate>


val empty: playlist 

val ofItems: seq<item> -> playlist

val toItems: playlist -> seq<item>

val updateMsgOfItemUpdate: itemUpdate -> Twc.Hic.PlaylistServerInterface.PlaylistUpdateMessage

val updateOfUpdateMsg: seq<Twc.Hic.PlaylistServerInterface.PlaylistUpdateMessage> -> update

val sub: int -> seq<string> -> playlist -> playlist

val buildUpdate: playlist -> playlist -> update

val applyUpdate: playlist -> update -> playlist

val pprintItem: item -> string

val pprintItemUpdate: itemUpdate -> string

val pprintUpdate: update -> string

val pprintPlaylist: playlist -> string
