﻿module PlaylistSimulator

open System.Threading

type item = Twc.Hic.PlaylistServerInterface.PlaylistItem


let threshold = 4.0 * 1000.0
let nullPeriod = System.TimeSpan.FromMilliseconds 0.0
let id = ref 0


let itemTime (dt: System.DateTime) idx =
  dt + System.TimeSpan.FromMilliseconds ((float idx) * threshold)


let timecodeOfTime (dt: System.DateTime) =
  let mutable tc = new Twc.Hic.PlaylistServerInterface.TimeValue ()
  tc.Hour <- dt.Hour
  tc.Minute <- dt.Minute
  tc.Second <- dt.Second
  tc.Frame <- (dt.Millisecond / 33)
  tc


let timecodeOfTimeSpan (s: System.TimeSpan) =
  let mutable tc = new Twc.Hic.PlaylistServerInterface.TimeValue ()
  tc.Hour <- s.Hours
  tc.Minute <- s.Minutes
  tc.Second <- s.Seconds
  tc.Frame <- (s.Milliseconds / 33)
  tc


let timecode (h, m, s, f) =
  let mutable tc = new Twc.Hic.PlaylistServerInterface.TimeValue ()
  tc.Hour <- h
  tc.Minute <- m
  tc.Second <- s
  tc.Frame <- f
  tc


let nextId () =
  let id' = !id
  id := id' + 1
  id'


let item (id, title, dur) =
  let it = new item ()
  let recKey = nextId ()
  it.ReconciliationKey <- recKey.ToString()
  it.Id <- id
  it.Title <- title
  it.Duration <- timecode dur
  it


let copyItem (src: item) =
  let item = new Playlist.item ()
  item.ItemType <- src.ItemType 
  item.ReconciliationKey <- src.ReconciliationKey
  item.Id <- src.Id
  item.Title <- src.Title
  item.StartTime <- src.StartTime
  item.Duration <- src.Duration
  item


let updatedItem (dt: System.DateTime) (item: item) =
  let id = nextId ()
  let item' = copyItem item
  item'.ReconciliationKey <- id.ToString ()
  item'.StartTime <- timecodeOfTime dt
  item' 


let plTemplate repeatCnt =
  let titles = 
    [
      ("ISTAR", "SNDN", (0, 0, 0, 1)); 
      ("IXL",   "LEON", (0, 0, 0, 1)); 
      ("IXL",   "LEOF", (0, 0, 0, 1)); 
      ("ISTAR", "FCST", (0, 2, 0, 0)); 
      ("ISTAR", "SNUP", (0, 0, 0, 1)); 
      ("IXL",   "DISP", (0, 0, 0, 1));
    ]
  let titles = List.concat (List.replicate repeatCnt titles)
  let items = List.map item titles
  items


let create (observer: Observer.IPlaylistObserver) =  
  let running = ref false
  let timer = ref (null: Timer)
  let pl = ref (plTemplate 5)
  let pltimes = ref []

  let setTimer () =
    let pltimes = !pltimes
    let dt = List.head pltimes 

    // BUG: if Now has moved past date time we'll be passing a bad value to the timer.
    let wait = (dt - System.DateTime.Now)
    (!timer).Change (wait, nullPeriod) |> ignore

  let doUpdate _ =
    printf "%A\n" System.DateTime.Now

    if !running then
      let pl' = !pl
      if not (List.isEmpty pl') then
        let it = List.head pl'
        let ittime = List.head !pltimes
        let t = itemTime ittime (List.length pl')
        pl := List.tail pl' @ [updatedItem t it]
        pltimes := List.tail !pltimes @ [t]     
        observer.Update (Playlist.SetItems pl')

        setTimer ()

  { new Twc.Util.Task.ITask with
    member x.Start () =
      if not !running then
        running := true
        timer := new Timer (new TimerCallback (doUpdate))
        pltimes := List.map (itemTime System.DateTime.Now) [1..(List.length !pl)]

        // BUG: map2 requires both lists to be of same length
        pl := List.map2 (fun dt e -> updatedItem dt e) !pltimes !pl
        doUpdate ()

    member x.Terminate () =
      if !running then
        running := false
        timer := null
        pltimes := []
        pl := []
  }