﻿module ClientMonitor

open Twc.Hic.PlaylistServerInterface
open System.ServiceModel
open Twc.Util


let private lObj = new obj()
let private monitored = ref Map.empty
let private statSetrs = ref Map.empty


let clientStatus () =
  !monitored
  |> Map.toSeq
  |> Seq.map (fun (_,fn) -> fn ())


let list () = !monitored


let setStatus connType id s =
  !statSetrs
  |> Map.tryFind (connType, string id)
  |> Option.iter (fun fn -> fn s)


let getStatus connType id =
  !monitored
  |> Map.tryFind (connType, string id)
  |> Option.map (fun fn -> fn ())


let monitor connType id wSize filters =
  let key = (connType, string id)
  let connectedOn = System.DateTime.Now
  let status = ref SubscriberStatus.Connecting
  let lastContact = ref System.DateTime.Now

  let setStatus s =
    lastContact := System.DateTime.Now
    status := s

  let getStatus () =
    new SubscriberInfo (
      Status = !status,
      LastContact = !lastContact,
      ConnectedOn = connectedOn,
      Name = string id,
      WindowSize = wSize,
      Filters = filters,
      ConnectionType = connType)

  lock lObj (fun () ->
    statSetrs := Map.add key setStatus !statSetrs
    monitored := Map.add key getStatus !monitored)

  ( fun upd ->
      match upd with
      | Playlist.Nothing       -> ()
      | Playlist.SetItems _    -> setStatus SubscriberStatus.SentItems
      | Playlist.UpdateItems _ -> setStatus SubscriberStatus.Updating
  )  
  |> Util.asSideEffect
  |> Observer.decorate


/// Stops monitoring of clients in Error or that have disconnected.
let flush () =
  lock lObj (fun () ->
    !monitored
    |> Seq.filter (fun t ->
        match t.Value().Status with
        | SubscriberStatus.Disconnected | SubscriberStatus.Error -> true
        | _                                                      -> false)

    |> Seq.iter (fun k ->
        let k = k.Key
        monitored := Map.remove k !monitored
        statSetrs := Map.remove k !statSetrs)
    )

