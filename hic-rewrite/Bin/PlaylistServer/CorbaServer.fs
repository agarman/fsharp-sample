﻿module CorbaServer


let create (port:int) =
  let chan = new Ch.Elca.Iiop.IiopChannel (port)
  System.Runtime.Remoting.Channels.ChannelServices.RegisterChannel (chan, false)

  let publisher, attach, detach, onError = MonitoredPublisher.create "corba"

  let server =
    { new Twc.Hic.Base.CorbaBridge.IPlaylistServerDelegate with
        member x.Ping () = ()

        member x.Attach (observerName: string, obs: PlayList.Observer, wSize: int16, filters: string[], id: byref<int>) =
          let id' = System.Guid.NewGuid().ToString().GetHashCode()

          let proxy =
            ClientProxy.create id' (int wSize) filters
              (Corba.itemListOfUpdate >> obs.setItems)
              (List.map (Corba.notificationOfItemUpdate) >> List.toArray >> obs.update)
              onError

          lock publisher (fun () -> attach id' (int wSize) filters proxy)
          id <- id'

        member x.Detach (id: int) = detach id
    }

  Twc.Hic.Base.CorbaBridge.PlayListSubjectImpl.StartServer("PlayListSubject", server) |> ignore

  { new Observer.IPlaylistObserver with
      member x.Update upd =  lock publisher (fun () -> publisher.Update upd)
  }