﻿module Observer


type IPlaylistObserver =
  abstract Update: Playlist.update -> unit


let sink =
  { new IPlaylistObserver with
    member x.Update upd = ()
  }


let decorate fn (subscriber: IPlaylistObserver) =
  { new IPlaylistObserver with
    member x.Update upd = subscriber.Update (fn upd)
  }


let makeObserver fn = decorate fn sink


let withPlaylist fn =
  let playlist = ref Playlist.empty
  fun upd ->
    let pl = Playlist.applyUpdate !playlist upd
    playlist := pl
    fn pl
    upd


let applyFilters windowSize filters subscriber =
  let playlist = ref Playlist.empty

  let calcUpdate upd =
    let oldpl = !playlist
    let pl = Playlist.applyUpdate !playlist upd
    let pl = Playlist.sub windowSize filters pl
    let upd = Playlist.buildUpdate oldpl pl
    playlist := pl
    upd

  decorate calcUpdate subscriber


let compositeObserver (observers: seq<IPlaylistObserver>) =
  { new IPlaylistObserver with
    member x.Update upd = observers |> Seq.iter (fun o -> o.Update upd)
  }

