﻿module WcfClient

open Twc.Hic
open System.ServiceModel


let create host port name windowSize filters watchdogMilli (observer: Observer.IPlaylistObserver) =
  let client = 
    { new PlaylistServerInterface.IPlaylistSubscriber with
        member x.SetItems pl = lock x (fun () ->
          List.ofSeq pl
          |> Playlist.SetItems
          |> observer.Update)

        member x.Update upd = lock x (fun () ->
          Playlist.updateOfUpdateMsg upd
          |> observer.Update)
    }

  let addr = sprintf "net.tcp://%s:%d/Twc/Hic/PlaylistServer" host port
  let endp = new EndpointAddress (addr)
  let factory = new DuplexChannelFactory<PlaylistServerInterface.IPlaylistServer> (client, new NetTcpBinding (), endp)

  let svr = ref null
  let connect () =
    svr := factory.CreateChannel ()
    (!svr).Subscribe (name, windowSize, Seq.toArray filters)

  let disconnect id =
    (!svr).Unsubscribe id
    svr := null

  let ping () = (!svr).Ping ()

  ConnectionMgr.create<string> watchdogMilli connect disconnect ping

