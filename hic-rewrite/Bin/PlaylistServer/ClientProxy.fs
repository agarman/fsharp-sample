﻿module ClientProxy

open Twc.Util.Logging.Logger


let create id wSize filters onSet onUpdate onError =
  let dosafe fn = Twc.Util.Util.dosafe fn (fun ex ->
    onError id 
    Log.Error "communication error with client '%A':%s\n%s" id ex.Message (ex.ToString ()))

  { new Observer.IPlaylistObserver with
      member x.Update upd =
        match upd with
        | Playlist.Nothing          -> ()
        | Playlist.SetItems pl      -> dosafe (fun () -> onSet pl)
        | Playlist.UpdateItems upds -> dosafe (fun () -> onUpdate upds)
  }

  |> Observer.applyFilters wSize filters

